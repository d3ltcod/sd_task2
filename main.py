"""
Distributed Systems Task 2
Authors: -Adam Alvarado Bertomeu
		 -Bryan Porras Blanch
Description:
Date:
"""

import pika, sys, random, yaml, os
import pywren_ibm_cloud as pywren

n_maps = 0

def map_slave(n_slave, maps):

	msg_v = []
	iterator = 0
	sent=False

	def callback(ch, method, properties, body):
		
		nonlocal msg_v
		nonlocal iterator
		nonlocal n_slave
		nonlocal maps
		nonlocal sent

		ch.basic_ack(delivery_tag = method.delivery_tag)

		body = body.decode('ascii')

		if body == "PERM":
			num = random.randint(0,1000)
			ch.basic_publish(exchange="slave_queues", routing_key='', body=str(num))
			sent=True
		
		else:
			msg_v.append(body)
			iterator += 1

			if iterator >= maps:
				ch.stop_consuming()
			
			if not (sent):
				ch.basic_publish(exchange="", routing_key='leader', body=str(n_slave))


	rabbit_url = pika.URLParameters(os.environ.get('RABBIT_URL', ''))
	connection = pika.BlockingConnection(rabbit_url)
	channel = connection.channel()

	channel.basic_publish(exchange='', routing_key="leader", body=str(n_slave))
	channel.basic_consume(callback, queue=str(n_slave))
	channel.start_consuming()
	connection.close()


	return msg_v

def map_master(maps):

	perm_v = []
	iterator = 0
	current = maps

	def callback(ch, method, properties, body):

		nonlocal perm_v
		nonlocal iterator
		nonlocal current

		ch.basic_ack(delivery_tag = method.delivery_tag)
		
		body = body.decode('ascii')
		perm_v.append(body)

		iterator += 1
		
		if iterator >= current:
			channel.stop_consuming()

	rabbit_url = pika.URLParameters(os.environ.get('RABBIT_URL', ''))
	connection = pika.BlockingConnection(rabbit_url)
	channel = connection.channel()
	channel.queue_declare(queue='leader')
	
	for i in range(0, current):
		perm_v = []
		iterator = 0
		
		channel.basic_consume(callback, queue="leader")
		channel.start_consuming()

		random_id = random.choice(perm_v)
		perm_v.remove(random_id)
		channel.basic_publish(exchange='', routing_key=str(random_id), body="PERM")
		current -= 1

	connection.close()



def main():

	if sys.argv[1] == "DELETE":

		with open(os.getenv("HOME") + "/.pywren_config" , 'r') as config_file:				# Configuration file
			res = yaml.safe_load(config_file)

		rabbit_url = res['rabbit']["url"]
		connection_params = pika.URLParameters(str(rabbit_url)) 		# Open a channel
		connection = pika.BlockingConnection(connection_params)
		channel = connection.channel()

		for i in range (0, int(sys.argv[2])):
			channel.queue_delete(queue=str(i))
	else:
		n_maps = int(sys.argv[1])
		with open(os.getenv("HOME") + "/.pywren_config" , 'r') as config_file:				# Configuration file
			res = yaml.safe_load(config_file)

		rabbit_url = res['rabbit']["url"]
		connection_params = pika.URLParameters(str(rabbit_url)) 		# Open a channel
		connection = pika.BlockingConnection(connection_params)
		channel = connection.channel()

		channel.exchange_declare(exchange="slave_queues", exchange_type="fanout")
		
		for i in range(0, n_maps):

			channel.queue_declare(queue=str(i), auto_delete=True)
			channel.queue_bind(exchange="slave_queues", queue=str(i))
		
		#Execute functions
		pw = pywren.ibm_cf_executor(rabbitmq_monitor=True);
		extra_env={'RABBIT_URL' : str(rabbit_url)}

		pw.call_async(func=map_master, data=n_maps, extra_env = extra_env, timeout=30)

		params_map_slave = []
		for i in range(0, n_maps):
			params_map_slave.append([i, n_maps])

		pw.map(map_slave, params_map_slave, extra_env = extra_env, timeout=30)
		results = pw.get_result()
		del results[0]
		
		for i in results:
		    print(str(i)+ "\n")

		connection.close()

main()