# Foundations of distributed systems using Pywren

The goal of this repo is to implement a synchronization system by using Cloud Serverless technologies. In concrete, by Using IBM-PyWren for running serverless functions and RabbitMQ as a message passing interface.

* [Serverless Data Analytics in the IBM Cloud](http://cloudlab.urv.cat/josep/distributed_systems/p1-Sampe.pdf)

Unlike Actor models, serverless Functions lack powerful addressing and communication mechanisms between them. In this event-driven model, functions must usually access other distributed services  to communicate , read inputs, write outputs, or even synchronize between them. Examples of these services are Object Stores like S3 or Azure Storage, NoSQL databases like Azure Cosmos DB or Amazon Dynamo DB, and in-memory stores like Azure Redis Cache, Amazon ElastiCache and IBM CloudAMQP. These third-party services will be used for synchronization, fault-tolerance, and consistency in many cases.

# Configure & Use
```
git clone git@bitbucket.org:d3ltcod/sd_task2.git
cd sd_task2
pip3 install pywren_ibm_cloud
pip3 install boto3
pip3 install pika
pip3 install pywren-ibm-cloud
# Configure pywren_ibm_cloud follow instructions on Repository. See Config File pywren and Libraries sections.
python3 main.py [NUMBER OF MAPS]
# or
python3 main.py DELETE [NUMBER OF MAPS]
```
# Config file pywren
You need to create in your home directory file **.pywren_config** that contains following structure:

```
pywren: 
    storage_bucket : <BUCKET NAME>

rabbitmq:
    amqp_url : <URL>

ibm_cf:
    endpoint    : <HOST>  # make sure to use https:// as prefix
    namespace   : <NAMESPACE>
    api_key     : <API_KEY>
   
ibm_cos:
    endpoint   : <REGION_ENDPOINT>  # make sure to use https:// as prefix
    access_key : <ACCESS_KEY>
    secret_key : <SECRET_KEY>
    
rabbit:
    url         : <AMPQ_URL>

```

# Libraries
* [pywren_ibm_cloud](https://github.com/pywren/pywren-ibm-cloud)
* [boto3](https://github.com/boto/boto3)

# Author

**Adam Alvarado Bertomeu** [d3ltcod](https://bitbucket.org/d3ltcod/)

**Bryan Porras Blanch** [kaddcoder](https://bitbucket.org/kaddcoder/)

# License
This project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).